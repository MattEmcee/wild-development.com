<div id="footerContainer">
	<div id="portfolioLinks">
		<h3>Portfolio:</h3>
		<ul class="footerList">
			<li><a href="gallery.php#videoportfolio">Videos</a></li>
			<li><a href="gallery.php">Kitchens and Baths</a></li>
			<li><a href="gallery.php">Entryway Flooring</a></li>
			<li><a href="gallery.php">Courtyards and Patios</a></li>
			<li><a href="gallery.php">Wet Bars and Fireplaces</a></li>
			<li><a href="gallery.php">Entertainment Centers</a></li>
			<li><a href="gallery.php">Home Offices and Studios</a></li>
		</ul>
	</div>
	<div id="moreInfo">
		<h3>More:</h3>
		<ul class="footerList">
			<li><a href="http://www.tourfactory.com/474745">Virtual Tour</a></li>
			<li><a href="about_us.php#articles">Articles</a></li>
			<li><a href="about_us.php#testimonials">Testimonials</a></li>
			<li><a href="http://blog.wild-development.com">Our Blog</a></li>
			<li><a href="http://twitter.com/wilddevelopment">Follow us on Twitter</a></li>
		</ul>
	</div>
	<div id="associatonsLinks">
		<h3>Associations:</h3>
		<ul class="footerList">
			<li><a href="http://thenationalbusinessexperts.com">The National Business Experts</a></li>
			<li><a href="http://www.azroc.gov">AZ Registrar of Contractors</a></li>
			<li><a href="http://azideacenter.com">Home Design &amp; Idea Center</a></li>
			<li><a href="http://www.maricopacountyhomeshows.com">Maricopa Home &amp; Garden Show</a></li>
		</ul>
	</div>
	<div id="pagesLinks">
		<h3>Pages:</h3>
		<ul class="footerList">
			<li><a href="index.php">Home</a></li>
			<li><a href="gallery.php">Gallery</a></li>
			<li><a href="about_us.php">About Us</a></li>
			<li><a href="faq.php">Facts and Questions</a></li>
			<li><a href="how_to_choose.php">How to Choose</a></li>
			<li><a href="contact_us.php">Contact Us</a></li>
		</ul>
	</div>
	<div id="copyright">
		<p>Copyright &copy; &nbsp;wild development Design &amp; Renovation. All rights reserved.</p>
	</div>
	<div id="mattEmcee">
		<p>Site design, development, and SEO by <a href="http://mattemcee.com" style="text-decoration:underline;">MattEmcee Fusion Media.</a></p>
		<img src="media/mattemcee.png" alt="MattEmcee Fusion Media" width="50"/>
	</div>
</div>