<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
			<!--BEGIN [crawl & cache meta]-->
	<meta http-equiv="expires" content="Fri, 14 Mar 2014 01:00:00 GMT">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link id="page_favicon" href="media/favicon.png" rel="icon" type="image/x-icon" />
	<link rel="canonical" href="http://wild-development.com" />
			<!--END [crawl & cache meta]-->	

<!--site title and description-->
	<title>wild development Design &amp; Renovation -- Portfolio</title>
	<meta name="description" content="wild development specializes in the remoldeling of kitchens and bathrooms, and we have an unabridged photo and video portfolio to give examples. All of our photos can also be found on Flickr."/>
	<meta name="keywords" content="Scottsdale, Arizona, Phoenix, Maricopa County, Greater Phoenix Area, Contract, General Contracting, Kitchen, Bathroom, Home, Design, Remodel, Renovation" />
	
<!--css stylesheets including external GOOGLE FONTS-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="assets/styles/style.css" type="text/css" media="screen" charset="utf-8">
	<link rel="stylesheet" href="assets/styles/gallery.css" type="text/css" media="screen" charset="utf-8" />
	<style type="text/css">
	  	body {
	  		background-color: #fff;
	  		max-width: 1500px;
	  		height: auto;
	  		padding: 0px;
	  		margin: 0px;
	  	}
	</style>
	  
<!--php includes style sheets-->
	<link rel="stylesheet" href="assets/styles/header.css" type="text/css"  />
	<link rel="stylesheet" href="assets/styles/contact.css" type="text/css" />
	<link rel="stylesheet" href="assets/styles/twitter.css" type="text/css"  />
	<link rel="stylesheet" href="assets/styles/footer.css" type="text/css"  />
	  
<!--universal scripts-->
	<script src="assets/js/libraries/jquery162_com.js" type="text/javascript"></script>
	<script src="assets/js/libraries/jquery_ui1815_com.js" type="text/javascript"></script>
	<script src="assets/js/libraries/jquery_easingUI.js" type="text/javascript"></script>
	<script src="assets/js/libraries/prototypejs16.js" type="text/javascript"></script>
	
<!--php includes scripts-->
	<script type="text/javascript" src="assets/js/libraries/jquery_gallery03.js"></script>
	<script type="text/javascript" src="assets/js/libraries/twitter.js"></script>
	
<!--page unique scripts-->
	<script src="assets/js/libraries/jquery_flickr.js" type="text/javascript"></script>
	
<!--google plus one-->
	<script type="text/javascript">
	  (function() {
	    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
	    po.src = 'https://apis.google.com/js/plusone.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
	  })();
	</script>
	
<!--google analytics-->
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-27155471-1']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>
	
<!--flickr photoset code-->
	<script type="text/javascript">
		$(document).ready(function() {
			$('#flickr').flickrGallery({
				api_key:'eb6dd88ac72ecb299dd637503bd2b9d7',
				photoset_ids: [
					'72157628287493833',
					'72157628088644622',
					'72157628287535541',
					'72157628295772319',
					'72157628343544167',
					'72157628340138243',
				],
				loading_msg:'Image Loading...',
			});
		});
	</script>
	
</head>
<body>

<!--begin all page content-->
	<div id="container">
	
<!--page header-->
		<div id="header">
			<?php include 'header.php'; ?>
		</div>
		
<!--begin content flow-->
		<div id="mainContent">

<!--photo gallery-->
			<div id="photoPortfolio">
				<h1 class="centerText">Photo Portfolio</h1>
				<div id="flickr"></div>
			</div>
			
<!--video lineup-->	
			<div id="videoPortfolio">
				<h1 class="centerText">Video Portfolio</h1>
				<div id="landingVideo">
					<iframe src="http://player.vimeo.com/video/31772441?title=1&amp;byline=0&amp;portrait=0&amp;color=983050" width="326" height="247" frameborder="0" webkitAllowFullScreen allowFullScreen>Renovation and Design Video Posted on Vimeo.com/wilddevelopment</iframe>
					<iframe src="http://player.vimeo.com/video/31772421?title=1&amp;byline=0&amp;portrait=0&amp;color=983050" width="326" height="247" frameborder="0" webkitAllowFullScreen allowFullScreen>Renovation and Design Video Posted on Vimeo.com/wilddevelopment</iframe>
					<iframe src="http://player.vimeo.com/video/31772373?title=1&amp;byline=0&amp;portrait=0&amp;color=983050" width="326" height="247" frameborder="0" webkitAllowFullScreen allowFullScreen>Renovation and Design Video Posted on Vimeo.com/wilddevelopment</iframe>
					<iframe src="http://player.vimeo.com/video/31772345?title=1&amp;byline=0&amp;portrait=0&amp;color=983050" width="326" height="247" frameborder="0" webkitAllowFullScreen allowFullScreen>Renovation and Design Video Posted on Vimeo.com/wilddevelopment</iframe>
					<iframe src="http://player.vimeo.com/video/31772321?title=1&amp;byline=0&amp;portrait=0&amp;color=983050" width="326" height="247" frameborder="0" webkitAllowFullScreen allowFullScreen>Renovation and Design Video Posted on Vimeo.com/wilddevelopment</iframe>
				</div>
			</div>

<!--end content flow-->
		</div>
		
<!--begin twitter feed bar-->
		<div id="twitterFeed">
			<?php include 'twitter_bar.php'; ?>
		</div>
		
<!--end all page content-->
	</div>
	
<!--page footer-->
	<div id="footer">
		<?php include 'footer.php'; ?>
	</div>
</body>
</html>