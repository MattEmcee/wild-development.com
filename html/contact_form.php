<script type="text/javascript">
	$(document).ready(function () {
		$('input').focus(function() {
			$(this).addClass('hasFocus');
		});
		$('input').blur(function () {
			$(this).removeClass('hasFocus');
		});
		$('textarea').focus(function() {
			$(this).addClass('hasFocus');
		});
		$('textarea').blur(function () {
			$(this).removeClass('hasFocus');
		});
	});
</script>
<div id="formArea">
	<h2 style="text-align:center;">fill out the form below to contact us directly:</h2>
	<form method="post" action="assets/contactengine.php">
		<fieldset>
			<label for="Name">Name:</label>
				<div id="inputBG">
					<input type="text" name="Name" id="Name" class="required" minlength="2" />
				</div>
			<label for="City">City:</label>
				<div id="inputBG">
					<input type="text" name="City" id="City" class="required" minlength="2" />
				</div>
			<label for="Tel">Day Phone:</label>
				<div id="inputBG">
					<input type="text" name="Tel" id="Tel" minlength="2" />
				</div>
			<label for="Email">Email:</label>
				<div id="inputBG">
					<input type="text" name="Email" id="Email" class="required email" />
				</div>
			<label for="Message">Notes and/or Questions:</label>
				<div id="inputBG">
					<textarea name="Message" id="Message" rows="10" cols="50" class="required"></textarea>
				</div>
			<div id="submitForm">
				<input type="image" id="submit" name="submit" value="submit" src="media/contact_form/letter.png" />
			</div>
		</fieldset>
	</form>
</div>