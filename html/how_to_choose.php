<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
			<!--BEGIN [crawl & cache meta]-->
	<meta http-equiv="expires" content="Fri, 14 Mar 2014 01:00:00 GMT">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link id="page_favicon" href="media/favicon.png" rel="icon" type="image/x-icon" />
	<link rel="canonical" href="http://wild-development.com" />
			<!--END [crawl & cache meta]-->	

<!--site title and description-->
	<title>wild development Design &amp; Renovation -- Choosing the Right Contractor</title>
	<meta name="description" content="wild development specializes in the remoldeling of kitchens and bathrooms, and we love to answer questions you may have about your design process. Contact us via our contact form, email, phone [480-256-WIILD], or by any number of the listed social networks where you can find us."/>
	<meta name="keywords" content="Scottsdale, Arizona, Phoenix, Maricopa County, Greater Phoenix Area, Contract, General Contracting, Kitchen, Bathroom, Home, Design, Remodel, Renovation" />
	
<!--css stylesheets including external GOOGLE FONTS-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="assets/styles/style.css" type="text/css" media="screen" charset="utf-8">
	<style type="text/css">
	  	body {
	  		background-color: #fff;
	  		max-width: 1500px;
	  		height: auto;
	  		padding: 0px;
	  		margin: 0px;
	  	}
	</style>
	  
<!--php includes style sheets-->
	<link rel="stylesheet" href="assets/styles/header.css" type="text/css"  />
	<link rel="stylesheet" href="assets/styles/contact.css" type="text/css" />
	<link rel="stylesheet" href="assets/styles/twitter.css" type="text/css"  />
	<link rel="stylesheet" href="assets/styles/footer.css" type="text/css"  />
	  
<!--universal scripts-->
	<script src="assets/js/libraries/jquery162_com.js" type="text/javascript"></script>
	<script src="assets/js/libraries/jquery_ui1815_com.js" type="text/javascript"></script>
	<script src="assets/js/libraries/jquery_easingUI.js" type="text/javascript"></script>
	<script src="assets/js/libraries/prototypejs16.js" type="text/javascript"></script>
	
<!--php includes scripts-->
	<script type="text/javascript" src="assets/js/libraries/jquery_gallery03.js"></script>
	<script type="text/javascript" src="assets/js/libraries/twitter.js"></script>
	
<!--google plus one-->
	<script type="text/javascript">
	  (function() {
	    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
	    po.src = 'https://apis.google.com/js/plusone.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
	  })();
	</script>
	
<!--google analytics-->
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-27155471-1']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>
</head>
<body>

<!--begin all page content-->
	<div id="container">
	
<!--page header-->
		<div id="header">
			<?php include 'header.php'; ?>
		</div>
		
<!--begin content flow-->		
		<div id="mainContent">

<a name="topOfPage"></a>
			<h1 style="border-bottom: 1px solid;">How to Choose a Professional General or Remodeling Contractor:</h1>
				
				<h2 class="centerText">Most Homeowners Only Hire a General or Remodeling Contractor Every 7 to 10 Years</h2>
					<p>The following information is provided to assist homeowners in choosing a General or Remodeling Contractor in the Phoenix, Scottsdale/Tempe Arizona area to add an addition, upgrade their kitchen &amp; bath or to complete other creative renovations. This is not an exhaustive list and all of the questions may not apply to your situation.<br />We will be happy to provide you with a complete copy of our consumer guide prior to our first meeting.<br />Here are 3 areas we recommend you consider in evaluating every general or remodeling contractor you talk to:</p> 
					
<!--page navigation-->
				<ul>
					<li><a href="#reputation">The Reputation and Business Stability of the Company</a></li>
					<li><a href="#experience">Their Experience and Methodology</a></li>
					<li><a href="#service">Their Service Before, During and After Your Project</a></li>
				</ul>
				
				<h2 class="centerText">Remember: Anybody Can Say Anything!</h2>
					<p>Get the answers to these questions in writing to ensure your contractor's policies, procedures and standards will protect you from a project filled with missed deadlines, hidden charges, and faulty workmanship.<br />For each area simply ask the questions and ask the remodeling contractor to provide the <em>written evidence</em> to support their answers.</p>
<br /><br /><br />	

<a name="reputation"></a>				
				<h3 class="centerText">Reputation and Business Stability of the General or Remodeling Company</h3>
					<ul>
						<li>Are you registered with the State of Arizona Registrar of Contractors and will you provide us with your license number?</li>
						<li>Will you supply a copy of your current insurance certificate?</li>
						<li>Will you provide a copy of your business  license for your current company?</li> 
				    	<li>Can you provide me with a reference list with at least 10 customers?</li>
						<li>Have you ever filed bankruptcy for this or any other company in which you held an ownership interest?</li>
					</ul>

<a name="experience"></a>				
				<h3 class="centerText">Experience and Methodology</h3>
					<ul>
						<li>Do you provide a written agreement and a price that is guaranteed for the work specified without exceptions?</li>
						<li>Will the owner of the company be on site to supervise the project?</li>
						<li>Do you have a written policy regarding smoking, drugs and alcohol by your crews?</li>
						<li>How much of a deposit do you require?</li>
						<li>Do you use employees or sub-contractors?</li>
						<li>Are all of your subcontractors required to sign a standard of quality agreement and carry all insurances required by the State of Arizona?</li>
					</ul>
					
<a name="service"></a>						
				<h3 class="centerText">Service Before, During and After the Project</h3>
					<ul>
						<li>Will you provide me your warranty in writing?</li>
						<li>Have you ever repaired a project that was out of warranty?</li>
						<li>What is your company phone policy?</li>
						<li>How often do you meet with homeowner to review job progress?</li>
						<li>We hope these questions give you the information you need to make the best choice when it comes to hiring, evaluating and working with a professional general or home remodeling contractor</li>
						<li>We will be happy to provide you with a complete copy of our consumer guide prior to our first meeting. We live by our guide. We know this sets a standard of which we can be proud and that the majority of our competitors can't match. Upon request we will gladly provide you with our written proof of compliance with each question.</li>
					</ul>
		
<!--end content flow-->
		</div>

<!--contact form bar-->
		<div id="contactForm">
			<?php include 'contact_form.php'; ?>
		</div>
		
<!--end all page content-->
	</div>
	
<!--page footer-->
	<div id="footer">
		<?php include 'footer.php'; ?>
	</div>
</body>
</html>