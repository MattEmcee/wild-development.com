<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
			<!--BEGIN [crawl & cache meta]-->
			
<!-- ////// NO INDEX ////// -->
	<meta name="robots" content="noindex">
<!--////// NO INDEX ////// -->
	
	<meta http-equiv="expires" content="0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link id="page_favicon" href="favicon.ico" rel="icon" type="image/x-icon" />
	<link rel="canonical" href="http://wild-development.com" />
			<!--END [crawl & cache meta]-->	

<!--site title and description-->
	<title>wild development Design &amp; Renovation -- Contact Us</title>
	<meta name="description" content="wild development is a professional, general contracting company based out of Scottsdale, Arizona and serving the greater Phoenix area that specializes in kitchen and bathroom designs."/>
	<meta name="keywords" content="Scottsdale, Arizona, Phoenix, Maricopa County, Greater Phoenix Area, Contract, General Contracting, Kitchen, Bathroom, Home, Design, Remodel, Renovation" />
	
<!--css stylesheets including external GOOGLE FONTS-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="assets/styles/style.css" type="text/css" media="screen" charset="utf-8">
	<style type="text/css">
	  	body {
	  		background-color: #fff;
	  		max-width: 1500px;
	  		height: auto;
	  		padding: 0px;
	  		margin: 0px;
	  	}
	</style>
	  
<!--php includes style sheets-->
	<link rel="stylesheet" href="assets/styles/header.css" type="text/css"  />
	<link rel="stylesheet" href="assets/styles/contact.css" type="text/css" />
	<link rel="stylesheet" href="assets/styles/twitter.css" type="text/css"  />
	<link rel="stylesheet" href="assets/styles/footer.css" type="text/css"  />
	  
<!--universal scripts-->
	<script src="assets/js/libraries/jquery162_com.js" type="text/javascript"></script>
	<script src="assets/js/libraries/jquery_ui1815_com.js" type="text/javascript"></script>
	<script src="assets/js/libraries/jquery_easingUI.js" type="text/javascript"></script>
	<script src="assets/js/libraries/prototypejs16.js" type="text/javascript"></script>
	
<!--php includes scripts-->
	<script type="text/javascript" src="assets/js/libraries/jquery_gallery03.js"></script>
	<script type="text/javascript" src="assets/js/libraries/twitter.js"></script>
	
<!--google plus one-->
	<script type="text/javascript">
	  (function() {
	    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
	    po.src = 'https://apis.google.com/js/plusone.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
	  })();
	</script>
	
<!--google analytics-->
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-27155471-1']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>
</head>
<body>

<!--begin all page content-->
	<div id="container">
	
<!--page header-->
		<div id="header">
			<?php include 'header.php'; ?>
		</div>

			<h1 class="centerText" style="color: #680900;">Whoops...Something seems to have gone amiss...</h1>
			<p class="centerText" style="color: #680900;">Try clicking on the 'contact us' menu item and resubmitting the form again. Sorry for the inconvenience.</p>

<!--end all page content-->
	</div>
	
<!--page footer-->
	<div id="footer">
		<?php include 'footer.php'; ?>
	</div>
</body>
</html>