<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
			<!--BEGIN [crawl & cache meta]-->
	<meta http-equiv="expires" content="Fri, 14 Mar 2014 01:00:00 GMT">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link id="page_favicon" href="media/favicon.png" rel="icon" type="image/x-icon" />
	<link rel="canonical" href="http://wild-development.com" />
			<!--END [crawl & cache meta]-->	

<!--site title and description-->
	<title>wild development Design &amp; Renovation -- FAQ</title>
	<meta name="description" content="wild development specializes in the remoldeling of kitchens and bathrooms, and we love to answer questions you may have about your design process. Contact us via our contact form, email, phone [480-256-WIILD], or by any number of the listed social networks where you can find us."/>
	<meta name="keywords" content="Scottsdale, Arizona, Phoenix, Maricopa County, Greater Phoenix Area, Contract, General Contracting, Kitchen, Bathroom, Home, Design, Remodel, Renovation" />
	
<!--css stylesheets including external GOOGLE FONTS-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="assets/styles/style.css" type="text/css" media="screen" charset="utf-8">
	<style type="text/css">
	  	body {
	  		background-color: #fff;
	  		max-width: 1500px;
	  		height: auto;
	  		padding: 0px;
	  		margin: 0px;
	  	}
	</style>
	  
<!--php includes style sheets-->
	<link rel="stylesheet" href="assets/styles/header.css" type="text/css"  />
	<link rel="stylesheet" href="assets/styles/contact.css" type="text/css" />
	<link rel="stylesheet" href="assets/styles/twitter.css" type="text/css"  />
	<link rel="stylesheet" href="assets/styles/footer.css" type="text/css"  />
	  
<!--universal scripts-->
	<script src="assets/js/libraries/jquery162_com.js" type="text/javascript"></script>
	<script src="assets/js/libraries/jquery_ui1815_com.js" type="text/javascript"></script>
	<script src="assets/js/libraries/jquery_easingUI.js" type="text/javascript"></script>
	<script src="assets/js/libraries/prototypejs16.js" type="text/javascript"></script>
	
<!--php includes scripts-->
	<script type="text/javascript" src="assets/js/libraries/jquery_gallery03.js"></script>
	<script type="text/javascript" src="assets/js/libraries/twitter.js"></script>
	
<!--google plus one-->
	<script type="text/javascript">
	  (function() {
	    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
	    po.src = 'https://apis.google.com/js/plusone.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
	  })();
	</script>
	
<!--google analytics-->
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-27155471-1']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>
</head>
<body>

<!--begin all page content-->
	<div id="container">
	
<!--page header-->
		<div id="header">
			<?php include 'header.php'; ?>
		</div>
		
<!--begin content flow-->		
		<div id="mainContent">
<br />
<a name="topOfPage"></a>
			<h1 class="noPadding">wild development Design &amp; Renovation</h1>
			
<!--page navigation-->
			<ul class="pageNavigation">
				<li style="clear:both;"><a href="#q1">Q: Are you licensed as a home improvement contractor with
                the State of Arizona?</a></li>
				<li style="clear:both;"><a href="#q2">Q: Can I obtain a "ball park" estimate over the phone or 
                via email?</a></li>
				<li style="clear:both;"><a href="#q3">Q: Is It Really Necessary To Check References?</a></li>
				<li style="clear:both;"><a href="#q4">Q: Will You Provide A Complete Lien Waiver?</a></li>
				<li style="clear:both;"><a href="#q5">Q: Will your insurance agent provide me with a copy 
                of your Certificate of Insurance?</a></li>
				<li style="clear:both;"><a href="#q7">Q: How Will I be Kept Informed About the Progress of 
                the Project?</a></li>
				<li style="clear:both;"><a href="#q8">Q: When You Start A Job, Do You Stay Until It's Done?</a></li>
				<li style="clear:both;"><a href="#q9">Q: Do You Require a Deposit to Get On The Schedule?</a></li>
				<li style="clear:both;"><a href="#q10">Q: Do I need to be home when you're working?</a></li>
				<li style="clear:both;"><a href="#q11">Q: How long does it take?</a></li>
				<li style="clear:both;"><a href="#q12">Q: What about bathrooms?</a></li>
				<li style="clear:both;"><a href="#q13">Q: Where will you clean up?</a></li>
				<li style="clear:both;"><a href="#q14">Q: Do you work with Home Owner's Associations?</a></li>
			</ul>
		
<a name="q1"></a>
			<h2>Are you licensed as a home improvement contractor with the State of Arizona?</h2>	
				<p>Yes. Our license number is #AZ ROC 224566. Contractors who are not licensed should be avoided as they are likely to expose you to unnecessary insurance risks and provide inferior service.<br />When someone contracts without a license, they endanger the health, safety and welfare of the public, undercut licensed contractors who operate legitimately and lawfully, and hurt the entire construction industry.</p> 
				<p>Also, when a homeowner hires someone who is not licensed, they have no recourse through the ROC complaint process or the Residential Contractors' Recovery Fund.</p>
				<a href="#topOfPage">Back to the top.</a> 
				
<a name="q2"></a>
			<h2>Can I obtain a "ball park" estimate over the phone or via email?</h2>
				<p>General or home remodeling contractors who will give you a quote over the phone without meeting you or visiting your property can be misleading. This is very unprofessional and these companies should be avoided.<br />We do not give estimates over the phone. There are too many variables to simply give an accurate quote. We will meet you in person, ask you specific questions to gain a full understanding of your project needs and then provide you with a written detailed fixed price bid with a complete breakdown and specific allowances so you can make an apples-to-apples comparison with any other bid you get.</p>
				<a href="#topOfPage">Back to the top.</a>
				
<a name="q3"></a>
			<h2>Is It Really Necessary To Check References?</h2>
				<p>Yes. Quality home remodeling contractors will have multiple references from past clients. We believe that a home remodeling contractor should provide you with a list of at least  10 references – this prevents the contractor from giving you 3 or 4 "references" that are actually not real customers but friends or associates. (Yes, it's been known to happen)<br />wild development Design &amp; Renovation provides a list of at least 10 references you can call. If a contractor is unable or unwilling to provide references that should be a warning sign.</p>
				<a href="#topOfPage">Back to the top.</a>
				
<a name="q4"></a>
			<h2>Will You Provide A Complete Lien Waiver?</h2>
				<p>To fully protect your home against financially unstable contractors, their subcontractors and suppliers – insist that a complete lien waiver be included in your written contract before you sign it.<br />At wild development Design &amp; Renovation we are proud to provide all of our clients with a complete lien waiver. In Plain English – we will protect and indemnify our clients from all liens by our subcontractors and suppliers. When you pay your final bill to us – you can sleep at night.</p>
				<a href="#topOfPage">Back to the top.</a>
				
<a name="q5"></a>
			<h2>Will your insurance agent provide me with a copy of your Certificate of Insurance?</h2>
				<p>Yes. Getting the Certificate of Insurance directly from the contractor's insurance agent will provide you with assurance that the coverage is current. <br />In addition to licensing by Arizona – quality General and home remodeling contractors adhere to the following guidelines:</p>
					<ul>
						<li>The Home Remodeling Contractor and/or their sub contractors are required to maintain statutory Worker's Compensation insurance.</li> 
						<li>If the Home Remodeling Contractor utilizes sub-contractors – they all must also meet the same insurance requirements. This is something that many General or Remodeling Contractors let slide and it can expose the homeowner to unnecessary risks.</li>
						<li>The Home Remodeling Contractor must carry a Contractor Bond and a Consumer Protection Bond.</li>
					</ul>
				<a href="#topOfPage">Back to the top.</a>
				
<a name="q7"></a>
			<h2>How Will I be Kept Informed About the Progress of the Project?</h2>
				<p>Every day we update you. This communication is an essential part of keeping your project on time and on budget.<br />Keeping you informed every day allows you to feel comfortable that progress is being made.<br />We'll even keep you updated if you're traveling with emails and photos!</p>
				<a href="#topOfPage">Back to the top.</a>
				
<a name="q8"></a>
			<h2>When You Start A Job, Do You Stay Until It's Done?</h2>
				<p>Yes. We are motivated to complete your project as that is the only way we get paid. Once we start your project, we stay on it each day until we are finished unless there are circumstances beyond our control like weather that impedes our ability to permit us to work.</p>
				<a href="#topOfPage">Back to the top.</a>
				
<a name="q9"></a>
			<h2>Do You Require a Deposit to Get On The Schedule?</h2>
				<p>We only require a $250 deposit to secure your place on our schedule. We know this is an act of good faith from us and from our customers.<br />Some of our competitors will require as much as 25% up front – sometimes more. <br />Unfortunately, the reason some contractors ask for that much up front is because they are strapped for cash and need your deposit to pay for the completion of some other project.</p>
				<a href="#topOfPage">Back to the top.</a>
				
<a name="q10"></a>
			<h2>Do I need to be home when you're working?</h2>
				<p>Not all the time but we do ask that you be available at some time during the day (email, phone, fax or texting will work too) each day so we can provide you with our daily update of progress and answer any questions you have.</p>
				<a href="#topOfPage">Back to the top.</a>
				
<a name="q11"></a>
			<h2>How long does it take?</h2>
				<p>Each project is different. We give you approximate start and finish dates in our proposal so you can plan. Once we start your project, we stay on it each day until we are finished unless there are circumstances beyond our control like weather that impedes our ability to permit us to work.</p>
				<a href="#topOfPage">Back to the top.</a>
				
<a name="q12"></a>
			<h2>What about bathrooms?</h2>
				<p>We do ask for access to a bathroom as a courtesy. We will respect your privacy and your property in using the bathroom. However, many of our clients prefer to have an on-site porta potty to address this concern.</p>
				<a href="#topOfPage">Back to the top.</a>
				
<a name="q13"></a>
			<h2>Where will you clean up?</h2>
				<p>We do ask for access to a sink that we can use for cleanup but will make other arrangements if that isn't possible.</p>
				<a href="#topOfPage">Back to the top.</a>
				
<a name="q14"></a>
			<h2>Do you work with Home Owner's Associations?</h2>
				<p>Yes. We will provide the appropriate information to your HOA.  We can handle the meetings with architectural committees for plan submittals and design review if it is required by your HOA.</p>
				<a href="#topOfPage">Back to the top.</a>
				
<!--end content flow-->
		</div>
	
<!--contact form bar-->
		<div id="contactForm">
			<?php include 'contact_form.php'; ?>
		</div>
		
<!--end all page content-->
	</div>
	
<!--page footer-->
	<div id="footer">
		<?php include 'footer.php'; ?>
	</div>
</body>
</html>