<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
			<!--BEGIN [crawl & cache meta]-->
	<meta http-equiv="expires" content="0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link id="page_favicon" href="media/favicon.png" rel="icon" type="image/x-icon" />
	<link rel="canonical" href="http://wild-development.com" />
			<!--END [crawl & cache meta]-->	

<!--site title and description-->
	<title>404 Error!</title>
	
<!--css stylesheets including external GOOGLE FONTS-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="assets/styles/footer.css" type="text/css"  />
	<link rel="stylesheet" href="assets/styles/style.css" type="text/css"  />
	<style type="text/css">
	  	body {
	  		background-color: #fff;
	  		min-width: 1000px;
	  		height: auto;
	  		padding: 0px;
	  		margin: 0px;
	  	}
	  	
	  	a {
	  		color: #983050;
	  		border: none;
	  	}
	  	
	  	#container {
	  		width: 900px;
	  		margin: 0 auto;
	  		padding: 0;
	  	}
	  	
	  	#logo {
	  		width: 400px;
	  		height: auto;
	  		margin: 0 auto;
	  		padding: 0;
	  	}
	  	
	  	#errorPNG {
	  		margin: 0 0 0 90px;
	  		padding: 0;
	  		float: left;
	  		clear: left;
	  	}
	  	
	  	#tagLine, #description {
	  		color: #680900;
	  		text-align: left;
	  		font-size: 18px;
	  		line-height: 22px;
	  		font-weight: bold;
	  		font-family: 'Kameron', serif;
	  	}
	  	
	  	#tagLine h1{
	  		float: left;
	  		width: 580px;
	  		line-height: 50px;
	  		margin: 15px 0 0 40px;
	  		padding: 0;
	  		clear: right;
	  	}
	  	
	  	#description {
	  		float: left;
	  		text-align: center;
	  		width: 100%;
	  		margin: 0;
	  	}
	</style>

<!--google analytics-->
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-27155471-1']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>
	
</head>
<body>

<!--begin all page content-->
 	<div id="container">
 	
 <!--error and logo images-->
 		<div id="logo">
 			<img src="media/PNGlogoColorLarge.png" alt="wild-development Design &amp; Renovation" height="250" />
 		</div>
 		<div id="tagLine">
	 		<div id="errorPNG">
	 			<img src="media/exclamation-mark.png" alt="404 Error!" width="125"/>
	 		</div>
	 		
<!--404 description text-->	 		
 			<h1>It appears that we have a 404 situation here. Bummer!</h1>
 		</div>
 		<div id="description">
 			<p>Don't take it personally, but you seem to have navigated to a non-existent page on our site.</p>
 			<p>Hey, chances are you simply typed the address wrong. BUT, if you think otherwise...<br /> Contact our <a class="mailto" 
 			href="mailto:matt@wild-development.com?subject=There's&nbsp;A&nbsp;Problem&nbsp;With&nbsp;At&nbsp;Wild-Development&#x2E;&#x2E;&#x2E;">web administrator</a> or feel free to simply give as a ring at 480.256.WILD(9453).</p>
 		</div>
 
 <!--end all page content-->
 	</div>
 	
 <!--page footer-->
 	<div id="footer">
 		<?php include 'footer.php'; ?>
 	</div>
</body>
</html>