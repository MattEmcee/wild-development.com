<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
			<!--BEGIN [crawl & cache meta]-->
	<meta http-equiv="expires" content="Fri, 14 Mar 2014 01:00:00 GMT">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link id="page_favicon" href="media/favicon.png" rel="icon" type="image/x-icon" />
	<link rel="canonical" href="http://wild-development.com" />
			<!--END [crawl & cache meta]-->	

<!--site title and description-->
	<title>wild development Design &amp; Renovation -- About US</title>
	<meta name="description" content="wild development is a professional, general contracting company based out of Scottsdale, Arizona and serving the greater Phoenix area that specializes in kitchen and bathroom designs."/>
	<meta name="keywords" content="Scottsdale, Arizona, Phoenix, Maricopa County, Greater Phoenix Area, Contract, General Contracting, Kitchen, Bathroom, Home, Design, Remodel, Renovation" />
	
<!--css stylesheets including external GOOGLE FONTS-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="assets/styles/style.css" type="text/css" media="screen" charset="utf-8">
	<style type="text/css">
	  	body {
	  		background-color: #fff;
	  		max-width: 1500px;
	  		height: auto;
	  		padding: 0px;
	  		margin: 0px;
	  	}
	</style>
	  
<!--php includes style sheets-->
	<link rel="stylesheet" href="assets/styles/header.css" type="text/css"  />
	<link rel="stylesheet" href="assets/styles/contact.css" type="text/css" />
	<link rel="stylesheet" href="assets/styles/twitter.css" type="text/css"  />
	<link rel="stylesheet" href="assets/styles/footer.css" type="text/css"  />
	  
<!--universal scripts-->
	<script src="assets/js/libraries/jquery162_com.js" type="text/javascript"></script>
	<script src="assets/js/libraries/jquery_ui1815_com.js" type="text/javascript"></script>
	<script src="assets/js/libraries/jquery_easingUI.js" type="text/javascript"></script>
	<script src="assets/js/libraries/prototypejs16.js" type="text/javascript"></script>
	
<!--php includes scripts-->
	<script type="text/javascript" src="assets/js/libraries/jquery_gallery03.js"></script>
	<script type="text/javascript" src="assets/js/libraries/twitter.js"></script>
	
<!--google plus one-->
	<script type="text/javascript">
	  (function() {
	    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
	    po.src = 'https://apis.google.com/js/plusone.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
	  })();
	</script>
	
<!--google analytics-->
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-27155471-1']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>
</head>
<body>

<!--begin all page content-->
	<div id="container">
	
<!--page header-->
		<div id="header">
			<?php include 'header.php'; ?>
		</div>

<!--begin content flow-->		
		<div id="mainContent">
<br />
<a name="topOfPage"></a>
			<h1 class="noPadding">wild development Design &amp; Renovation</h1>

<!--page navigation-->			
				<ul class="pageNavigation">
					<li><a href="#whoIs">About wild development</a></li>
					<li><a href="#maryWild">Mary Wild; owner &amp; president</a></li>
					<li><a href="#setsApart">Our Dedication</a></li>
					<li><a href="#blog">Updates from Our Blog</a></li>
					<li><a href="#articles">Press Articles</a></li>
					<li><a href="#testimonials">Testimonials</a></li>
				</ul>

<!--about us descriptions-->
<a name="whoIs"></a>
			<h2>Who is wild development and How Can They Help Me?</h2>
			
				<p>wild development Design &amp; Renovation is a woman-owned business of remodeling professionals serving homeowners in the Phoenix Metropolitan area including Scottsdale, Paradise Valley &amp; Tempe. We have built our reputation on honesty, integrity, reliability, professionalism and customer satisfaction.</p>
				<p>We recognize that most people experience some anxiety looking for a general or remodeling contractor and conceptualizing a project from design to completion. We pride ourselves on helping people visualize through photos, design sketches and actual finish materials what the final end product will look like. We'd like to show you how we are doing something unique in our industry. It is our goal that every project becomes a very positive experience.<br /><a href="#topOfPage">Back to the top of the page</a></p>
				
<a name="maryWild"></a>
			<h2><span>About Mary Wild; owner and president of</span><br /><span> wild development Design &amp; Renovation:</span></h2>
			
				<p><img src="media/maryHeadshot.png" alt="headshot" align="left" style="margin: 0 10px 0 0;" />"My name is Mary Wild, and as the owner and president of wild development, I want to assure you that doing business with my company will be a very positive experience. Our team does something unique in our business….we eliminate most of the problems people traditionally have trying to hire, evaluate and work with a general or home remodeling contractor.</p>
				<p>I know our industry is competitive and I appreciate this opportunity to earn your business. I realize what it takes to get your business, what it takes to give you the results you demand and what needs to be done to eliminate many of the common headaches and frustrations people have with a remodeling project.</p></span>
				<p>Thanks for your time and I look forward to answering your questions about your home remodeling project.</p>
				<p><br />Sincerely,<br />Mary Wild - President<br /><a href="#topOfPage">Back to the top of the page</a></p>
				
<a name="setsApart"></a>
			<h2>What Sets wild development Apart From the Rest?</h2>
			
				<p><em>We take a different approach to our projects and address many aspects of the process that other contractors don't to take the time to deal with, for example:</em></p>
				<ul>
					<li>Many remodeling contractors will simply send you off to their suppliers and let you pick out your materials on your own. They don't provide you with any guidance on look, use, design, cost, quality or functionality of the product or material to be used in your home. This makes it almost impossible for your project to stay within budget or to maintain a consistent and coordinated look.</li><br />
					<li>If you've never selected cabinets, appliances, countertop or flooring material, hardware, paint, doors plumbing or electrical fixtures etc., it can seem overwhelming. You may not even be aware of the important factors that should be considered in making these selections. It's easy to make costly mistakes without a little guidance.</li><br />
					<li>We take the time to listen to your needs, goals and desires and then will help make your job easier by providing specific recommendations alternatives for your consideration. This helps to ensure that the finished product will meet your needs from a design and functional perspective as well as staying within your budget parameters.</li><br />
					<li>This is just one of the ways we can keep you within budget and take the guesswork and frustration out of the many choices you will make for your project.</li>
				</ul>
				<p><a href="#topOfPage">Back to the top of the page</a></p>
						
<a name="blog"></a>
			<h2>Recent Updates from Our Blog:</h2>
			
				<p>This feature is still underway. For now, visit our blog directly by going to <a href="http://blog.wild-development.com">http://blog.wild-development.com</a><br /><a href="#topOfPage">Back to the top of the page</a></p>
				<div id="blogRole"></div>
						
			<h2>Testimonials:</h2>
			
<a name="articles"></a>			
				<h3 class="centerText noPadding">Recent Articles:</h3>
<br />			
					<h4 class="noPadding">The Following Article was Published by QualityBath.com on Sept 19, 2011:</h4>
						<p>"'Your project is not about the architect, builder, or the designer’s vision…it’s about YOUR vision'…Wouldn’t it be a dream come true to work with a contractor who lived by these words? Sadly it doesn’t happen often, but occasionally you will hit the jackpot. We did when we discovered Wild Development, Design &amp; Renovation."</p> 
						<p>"...Since Mary herself is a mom, she understands how important it is for families with children to have a place to do homework, a mission control area etc. ...At Wild Development each client is dealt with exclusively to make sure their unique needs are met to best accommodate their lifestyle." -- AlizaK of QualityBath.com</p>
						<p><a href="http://qualitybath.com/blog/design-inspiration/wild-development-where-your-dreams-become-reality">Click for the full article posted on the QualityBath.com blog.</a></p>
						<p><a href="#topOfPage">Back to the top of the page</a></p>
<br /><br />
<a name="testimonials"></a>
				<h3 class="centerText">Customer Testimonials:</h3>
				
					<p>"wild development Design &amp; Renovation was the General Contractor for an extensive remodel of our home. Prior to hiring Mary Wild, we interviewed several other designers and contractors. None really had the vision to do what we wanted, especially with the master bathroom. Mary showed creativity and did an excellent job. She completely updated our home including the complete remodel of the kitchen, three (3) bathrooms and more. We are extremely pleased with the result."</p><p><em>- Lee S., Tempe AZ</em></p>
<br />
					<p>"An important lesson learned in Corporate America was “handle the task as if you owned the business.” This fits wild development to a “T”. Mary adopted that stance while remodeling our bathroom in Scottsdale. Her creativity, insistence on quality, professionalism and adherence to our budget resulted in a job EXTREMELY WELL DONE! She handled our project as if the home was hers. Acid test: Would I call on Mary for future projects? You betcha!"</p><p><em>- Russ D., Scottsdale AZ</em></p>
<br />
					<p>"The first time I used wild development, was when I decided to perform a complete remodel including major structural changes to an older home in Scottsdale Ranch. My project was completed within budget, on time and the results were amazing. Her creativity and attention to detail made the home truly unique. I received so many compliments. Eventually I sold the home for full listing price after it was on the market for only one (1) day. The buyer was a Realtor himself and recognized the quality of the work to be superb.<br />I then purchased another home and when I need to make changes and update, I immediately called Mary. Once again I was delighted."</p><p><em>- Lori P., Scottsdale AZ</em></p>
<br />
					<p>"wild development provided a timely and no BS quote. The work was performed timely and was of the best quality. What more could one ask for? I am now doing a full kitchen remodel and great room addition and I have engaged Mary’s services for this project as well."</p><p><em>- James C., Phoenix AZ</em></p>
<br />
					<p>"I purchased a home that was built in the 70’s. Unfortunately the previous owners hadn’t updated the house – but the house had great bones. So I decided to try to make the improvements myself without the help of a General Contractor. After spending too much and accomplishing too little, I called in wild development.<br />Ms. Wild listened, assessed and formulated a remodeling plan based on our agreed upon budget. The plan came together because Ms. Wild was able to translate my desire to open up the floor plan to create a great room concept with a high tech kitchen. Her use of color, paint and textures transformed a run down house into a warm comfortable home I was able to retreat to.<br />Oh yes, the best part, wild development brought the project in on time and on budget."<p><em>- Jeff T., Former Scottsdale Resident</em></p>
<br />
					<p>"As a client of wild development, I highly recommend them for any of your construction and design needs. They completed a remodel of my condominium complex clubhouse on time and on budget. Their design and construction team are very professional and highly skilled.<br />I would use them again, without hesitation."</p><p><em>- T. Testa, Board President; Biltmore Terrace Condominium Association, Phoenix, AZ</em></p>
<br />
					<p>"We hired wild development to help on a custom home build. Although we had already hired another builder, we were overwhelmed with contractors who didn’t show up or do what they said they would do. Mary Wild brought a reliable team, ideas for cost cutting, a great eye for design, and organization to the project. I have recommended wild development to friends and business clients, and without exception, they have been very happy too."</p><p><em>- Dave J., Scottsdale AZ</em></p>
<br />
					<p>"Mary (wild development) assisted us in remodeling our kitchen. We needed someone with the experience and ability to offer design ideas as well as coordinate the entire project. We started with wanting to just remodel the kitchen . However, with her vision, we also ended up expanding our family room into a Great Room and incorporated a Wet Bar. The room is perfect for entertaining family and friends and we have received multiple compliments. We also upgraded the flooring and paint throughout the house which provided a nice facelift for our home. She provided great ideas, prompt and courteous workers and we love our new kitchen. We encountered two issues but she immediately corrected them and kept the project on schedule . I would highly recommend Mary for any project. Feel free to contact me directly if you wish to discuss our kitchen remodeling experience."</p><p><em>- Scott B., Tempe AZ</em><br /><a href="#topOfPage">Back to the top of the page</a></p>
					
<!--end content flow-->				
		</div>
		
<!--contact form bar-->
		<div id="contactForm">
			<?php include 'contact_form.php'; ?>
		</div>
		
<!--end all page content-->
	</div>
	
<!--page footer-->
	<div id="footer">
		<?php include 'footer.php'; ?>
	</div>
</body>
</html>