<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<!-- HEAT MAP SNIPPET ****** PLEASE REMOVE -->
<script type="text/javascript" src="http://wild-development.com/heat-map/js/clickheat.js"></script><noscript><p><a href="http://www.dugwood.com/index.html">Open Source Sofware</a></p></noscript><script type="text/javascript"><!--
clickHeatSite = 'wild-development.com';clickHeatGroup = 'wddr';clickHeatServer = 'http://wild-development.com/heat-map/click.php';initClickHeat(); //-->
</script>
<!-- HEAT MAP SNIPPET ****** PLEASE REMOVE -->


			<!--BEGIN [crawl & cache meta]-->
	<meta http-equiv="expires" content="Fri, 14 Mar 2014 01:00:00 GMT">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link id="page_favicon" href="media/favicon.png" rel="icon" type="image/x-icon" />
	<link rel="canonical" href="http://wild-development.com" />
			<!--END [crawl & cache meta]-->	

<!--site title and description-->
	<title>Scottsdale Home Remodeling -- wild development Design &amp; Renovation</title>
	<meta name="description" content="wild development is a professional, general contracting company based out of Scottsdale, Arizona and serving the greater Phoenix area that specializes in kitchen and bathroom designs."/>
	<meta name="keywords" content="Scottsdale, Arizona, Phoenix, Maricopa County, Greater Phoenix Area, Contract, General Contracting, Kitchen, Bathroom, Home, Design, Remodel, Renovation" />
	
<!--css stylesheets including external GOOGLE FONTS-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="assets/styles/style.css" type="text/css" media="screen" charset="utf-8">
	<style type="text/css">
	  	body {
	  		background-color: #fff;
	  		height: auto;
	  		padding: 0px;
	  		margin: 0px;
	  	}
	</style>

<!--php includes style sheets-->
	<link rel="stylesheet" href="assets/styles/header.css" type="text/css"  />
	<link rel="stylesheet" href="assets/styles/contact.css" type="text/css" />
	<link rel="stylesheet" href="assets/styles/twitter.css" type="text/css"  />
	<link rel="stylesheet" href="assets/styles/footer.css" type="text/css"  />
	  
<!--universal scripts-->
	<script src="assets/js/libraries/jquery162_com.js" type="text/javascript"></script>
	<script src="assets/js/libraries/jquery_ui1815_com.js" type="text/javascript"></script>
	<script src="assets/js/libraries/jquery_easingUI.js" type="text/javascript"></script>
	
<!--php includes scripts-->
	<script type="text/javascript" src="assets/js/libraries/jquery_gallery03.js"></script>
	<script type="text/javascript" src="assets/js/libraries/twitter.js"></script>
	
<!--google plus one-->
	<script type="text/javascript">
	  (function() {
	    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
	    po.src = 'https://apis.google.com/js/plusone.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
	  })();
	</script>
	
<!--google analytics-->
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-27155471-1']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>
	
<!--google site verification-->
	<meta name="google-site-verification" content="ynkGTTYl2BN_DLPwKAQHoq-Lun82HULRmW7y0yGRfX8" />
</head>
<body>

<!--begin all page content-->
	<div id="container">
	
<!--page header-->
		<div id="header">
			<?php include 'header.php'; ?>
		</div>
		
<!--begin main content-->	
		<div id="mainContent">
		
			<h1>wild development Design &amp; Renovation</h1>
			
				<p>We are a team of residential design and remodeling experts serving the greater Phoenix Metropolitan area. We specialize in helping clients conceptualize their project from design to completion through the use of photos, individualized design sketches, including the selection of finishing materials.
	<br/>
	<br/>
				<a href="#landingVideo">Watch our project portfolio video</a></p>
		
			<h1>Mission Statement:</h1>
			
				<p>"At wild development Design &amp; Renovation our reputation continues to grow because; we believe that your project is not about the architect, builder or designer’s vision…..it’s about <em>your</em> vision. By working hand in hand with our clients, through every phase of your project, we deliver a quality product, on time and on budget."</p>
				<p>We encourage you to carefully review our site and then call us at 480-256-WILD(9453) for a complete copy of our consumer guide and remodeling process prior to our meeting. If you're worried about the cost of your project and getting the most for your money, this information will provide you with an advantage in evaluating, hiring and working with a construction or home remodeling contractor.</p>
				
<!--demo video-->	
		<div id="landingVideo">
			<iframe src="http://player.vimeo.com/video/31772475?title=0&amp;byline=0&amp;portrait=0&amp;color=983050" width="660" height="495" frameborder="0" webkitAllowFullScreen allowFullScreen>Demo Video Posted on Vimeo.com/wilddevelopment</iframe>
		</div>
		
<!--end main content-->
		</div>
		
<!--twitter feed sidebar-->
		<div id="contactForm">
			<?php include 'contact_form.php'; ?>
		</div>
		
	
<!--end all page content-->
	</div>

<!--page footer-->
	<div id="footer">
		<?php include 'footer.php'; ?>
	</div>
</body>
</html>
