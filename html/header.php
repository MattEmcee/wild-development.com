<script type="text/javascript">
	$(document).ready(function() {
		$('#gallery').gallery({
		  width: null,
		  height: null,
		  thumbWidth: 0,
		  thumbHeight: 0,
		  zIndex: 500,
		  interval: 6000,
		  prefix: 'gallery_',
		  easing: 'linear',
		  ratio: 0.15,
		  slideshow: true,
		  toggleBar: true,
		  showOverlay: false,
		  barPosition: null,
		  barClass: 'galleryBar',
		  contentClass: 'galleryContent',
		  infoClass: 'galleryInfo',
		  screenClass: 'galleryScreen',
		  titleClass: 'galleryTitle',
		  descClass: 'galleryDesc',
		});
	});
</script>
<div id="socialNetBar">
	<ul class="social">
		<li id="plusOne"><g:plusone href="http://wild-development.com" size="standard" annotation-"none" count="false"></g:plusone></li>
		
		<li><a href="http://facebook.com/wilddevelopment" title="Facebook">
			<img src="media/social/facebook.png" title="wild development on Facebook" /></a></li>
			
		<li><a href="http://flickr.com/photos/wild-development" title="Flickr">
			<img src="media/social/flickr.png" title="wild development on Flickr" /></a></li>
			
		<li><a href="http://foursquare.com/v/wild-development-llc/4e935b9461af7a8836d4e834" title="Foursquare">
			<img src="media/social/foursquare.png" title="wild development on Foursquare" /></a></li>
			
		<li><a href="http://plus.google.com/115963012125911543971/posts" title="Google Plus">
			<img src="media/social/google.png" title="wild development on Google Plus" /></a></li>
			
		<li><a href="http://linkedin.com/pub/mary-wild/14/8b6/a91" title="Linkedin">
			<img src="media/social/linkedin.png" title="wild development via Linkedin" /></a></li>
			
		<li><a href="http://blog.wild-development.com" title="Tumblr">
			<img src="media/social/tumblr.png" title="wild development on Tumblr" /></a></li>
			
		<li><a href="http://twitter.com/wilddevelopment" title="Twitter">
			<img src="media/social/twitter.png" title="wild development on Twitter" /></a></li>
			
		<li><a href="http://vimeo.com/wilddevelopment" title="Vimeo">
			<img src="media/social/vimeo.png" title="wild development on Vimeo" /></a></li>
			
		<li><a href="http://yelp.com/biz/wild-development-scottsdale" title="Yelp">
			<img src="media/social/yelp.png" title="wild development on Yelp" /></a></li>
			
		<li><a href="http://youtube.com/user/wilddevelopment" title="YouTube">
			<img src="media/social/youtube.png" title="wild development on YouTube" /></a></li>
	</ul>
</div>
<div id="gallery" class="gallery">
	<ul class="galleryBar">
		<li><a href="media/header_imgs/img1.png" title="Custom home, designed and constructed by wild development; Scottsdale, AZ"><img src="media/header_imgs/img1.png" title="Family View" /></a></li>
		<li><a href="media/header_imgs/img2.png" title="Custom home, designed and constructed by wild development; Scottsdale, AZ"><img src="media/header_imgs/img2.png" title="Front Entry" /></a></li>
		<li><a href="media/header_imgs/img3.png" title="Custom home, designed and constructed by wild development; Scottsdale, AZ"><img src="media/header_imgs/img3.png" title="Guest Bath" /></a></li>
		<li><a href="media/header_imgs/img4.png" title="Custom home, designed and constructed by wild development; Scottsdale, AZ"><img src="media/header_imgs/img4.png" title="Kitchen" /></a></li>
		<li><a href="media/header_imgs/img5.png" title="Custom home, designed and constructed by wild development; Scottsdale, AZ"><img src="media/header_imgs/img5.png" title="Kitchen Detail" /></a></li>
		<li><a href="media/header_imgs/img6.png" title="Custom home, designed and constructed by wild development; Scottsdale, AZ"><img src="media/header_imgs/img6.png" title="Master Spa" /></a></li>
		<li><a href="media/header_imgs/img7.png" title="Custom home, designed and constructed by wild development; Scottsdale, AZ"><img src="media/header_imgs/img7.png" title="Wet Bar" /></a></li>
		<li><a href="media/header_imgs/img8.png" title="Custom home, designed and constructed by wild development; Scottsdale, AZ"><img src="media/header_imgs/img8.png" title="Back Patio" /></a></li>
	</ul>
<img width="175"id="logo" src="media/header_imgs/wild-development_logo.png" alt="wild development Design &amp; Renovation; home remodel and design" />
</div>
<div id="slogan">
	<h2><span id="logoTextLeft" class="centerText">wild development<br />Design &amp; Renovation</span>
	<span id="logoTextRight" class="centerText" style="font-size:21px;line-height:32px;">(480) 256 - WILD &nbsp;[9453]</span></h2>
</div>
<ul class="menu">
	<a href="index.php"><li>Home</li></a>
	<a href="gallery.php"><li>Gallery</li></a>
	<a href="about_us.php"><li>About Us</li></a>
	<a href="about_us.php#blog"><li>Testimonials</li></a>
	<a href="faq.php"><li>FAQ</li>
	<a href="how_to_choose.php"><li>How to Choose</li>
	<a href="contact_us.php"><li>Contact Us</li></a>
</ul>